#!/bin/bash
version=0.15
zipBit=0

C='\e[36m'
G='\e[92m'
R='\e[31m'
Y='\e[33m'
X='\e[0m'

clear

if [[ -d "/root/backup_zeus" ]]; then
	echo -ne "$R"
	echo -e "THE DIRECTORY: ~/backup_zeus ALREADY EXISTS!$X"
	echo "Do you wish to remove it? (y/N)"
	read ans

	case $ans in
		y|yes|Y|YES)
			rm -r /root/backup_zeus
		;;
		n|no|N|NO)
			echo -ne "$Y"
			echo -e "ABORTED BY USER$X\n"
		;;
	esac
fi

echo -e "$R--- WARNING! ---------------------------------------$Y"
echo -e "- MAKE SURE YOU ARE ROOT & EXECUTE FROM ROOT's HOME DIRECTORY."
echo -e "- MAKE SURE THAT YOUR IPTABLES FILE IS LOCATED @ /root/iptables.sh"
echo -e "$R----------------------------------------------------$X"
echo -e "Proceed with backup of your config files? (y/N)"
read ans


backup() {
	echo -ne "$C"
	
	# CREATING DIRECTORIES --------------------------------------- 
	
	# DIR - BACKUP_ZEUS	
	echo -ne "Creating dir: ~/backup_zeus"
	mkdir ~/backup_zeus &> /dev/null
	echo -e "$G ...DONE!$C"

	# DIR - DEFAULT
	echo -ne "Creating dir: ~/backup_zeus/etc/default"
	mkdir -p ~/backup_zeus/etc/default &> /dev/null
	echo -e "$G ...DONE!$C"

	# DIR - BIND
	echo -ne "Creating dir: ~/backup_zeus/etc/bind"
	mkdir -p ~/backup_zeus/etc/bind &> /dev/null
	echo -e "$G ...DONE!$C"

	# DIR - DOVECOT
	echo -ne "Creating dir: ~/backup_zeus/etc/dovecot"
	mkdir -p ~/backup_zeus/etc/dovecot &> /dev/null
	echo -e "$G ...DONE!$C"
	
	# DIR - DHCP
	echo -ne "Creating dir: ~/backup_zeus/etc/dhcp"
	mkdir -p ~/backup_zeus/etc/dhcp &> /dev/null
	echo -e "$G ...DONE!$C"
	
	# DIR - POSTFIX
	echo -ne "Creating dir: ~/backup_zeus/etc/postfix"
	mkdir -p ~/backup_zeus/etc/postfix &> /dev/null
	echo -e "$G ...DONE!$C"

	# DIR - SQUID
	echo -ne "Creating dir: ~/backup_zeus/etc/squid"
	mkdir -p ~/backup_zeus/etc/squid &> /dev/null
	echo -e "$G ...DONE!$C"

	# DIR - NETWORK
	echo -ne "Creating dir: ~/backup_zeus/etc/network/"
	mkdir ~/backup_zeus/etc/network &> /dev/null
	echo -e "$G ...DONE!$C"
	
	# DIR - ROOT HOME DIRECTORY
	echo -ne "Creating dir: ~/backup_zeus/root"
	mkdir ~/backup_zeus/root &> /dev/null
	echo -e "$G ...DONE!$C"
	
	echo -e "$R----------------------------------------------------$C"

	# COPYING FILES -------------------------------------------------- 

	# COPYING - IPTABLES
	echo -ne "Copying 'iptables.sh'..."
	cp -r ~/iptables.sh ~/backup_zeus/root/ &> /dev/null
	echo -e "$G ...DONE!$C"

	# COPYING - BIND
	echo -ne "Copying BIND config files..."
	cp -r /etc/bind/* ~/backup_zeus/etc/bind/ &> /dev/null
	cp -r /etc/default/bind9 ~/backup_zeus/etc/default/ &> /dev/null
	echo -e "$G ...DONE!$C"

	# COPYING - DOVECOT
	echo -ne "Copying DOVECOT config files..."
	cp -r /etc/dhcp/* ~/backup_zeus/etc/dovecot/ &> /dev/null
	echo -e "$G ...DONE!$C"

	# COPYING - DHCP
	echo -ne "Copying DHCP config files..."
	cp -r /etc/dhcp/* ~/backup_zeus/etc/dhcp/ &> /dev/null
	echo -e "$G ...DONE!$C"

	# COPYING - POSTFIX
	echo -ne "Copying POSTFIX config files..."
	cp -r /etc/squid/* ~/backup_zeus/etc/postfix/ &> /dev/null
	echo -e "$G ...DONE!$C"
	
	# COPYING - SQUID
	echo -ne "Copying SQUID config files..."
	cp -r /etc/squid/* ~/backup_zeus/etc/squid/ &> /dev/null
	echo -e "$G ...DONE!$C"
	
	# COPYING - /etc/network/interfaces
	echo -ne "Copying '/network/interfaces' config file..."
	cp -r /etc/network/interfaces ~/backup_zeus/etc/network/ &> /dev/null
	echo -e "$G ...DONE!$C"
	
	echo -e "$R----------------------------------------------------$G"
	
	echo -e "BACKUP COMPLETE!$X"
}


zipping() {
	echo -ne "$Y"

	if [[ -f "/root/bk_zeus.zip" ]]; then
		echo -ne "$R"
		echo -e "$R----------------------------------------------------"
		echo -e "THE FILE: ~/bk_zeus.zip ALREADY EXISTS!$Y"
		echo -e "Do you wish to remove it? (y/N)$X"
		read ans
	
		case $ans in
			y|yes|Y|YES)
				rm -r /root/bk_zeus.zip
			;;
			n|no|N|NO)
				echo -ne "$Y"
				echo -e "ABORTED BY USER$X\n"
			;;
		esac
	fi

	echo -e "$R----------------------------------------------------$X"
	echo -e "Zip the contents of '~/backup_zeus' to 'bk_zeus.zip'? (y/N)"
	read opt
	
	case $opt in
		y|yes|Y|YES)
			zipBit=1
			zip -r bk_zeus.zip backup_zeus/*

		;;
		n|no|N|NO)
			echo -ne "$Y"
			echo -e "ABORTED BY USER$X"
		;;
	esac
}

change(){
	echo -e "$R----------------------------------------------------$G"
	if [[ $zipBit == '1' ]]; then
		echo -ne "$X"
		echo -e "Move 'bk_zeus.zip' to your user directory? (y/N)$X"
		read ans
		
		case $ans in
			y|yes|Y|YES)
				echo -e "$R----------------------------------------------------$Y"
				echo -ne "Enter the name of the user account: $X"
				read username
				mv /root/bk_zeus.zip /home/$username/
				chown $username:$username /home/$username/bk_zeus.zip
				echo -e "$R----------------------------------------------------$X"
				echo -e "Your file is now at:$C"
				ls -lh /home/$username/bk_zeus.zip
				echo -e "$R----------------------------------------------------$C"
			;;
			n|no|N|NO)
				echo -ne "$Y"
				echo -e "ABORTED BY USER$X"
			;;
		esac
	else
		echo -ne "$X"
		echo -e "Move 'backup_zeus/' to your user directory? (y/N)$X"
		read ans
		
		case $ans in
			y|yes|Y|YES)
				echo -e "$R----------------------------------------------------$Y"
				echo -ne "Enter the name of the user account: $X"
				read username
				mv /root/backup_zeus /home/$username/
				chown $username:$username /home/$username/backup_zeus
				echo -e "$R----------------------------------------------------$X"
				echo -e "Your folder is now at:$C"
				ls -lh /home/$username/backup_zeus
				echo -e "$R----------------------------------------------------$C"
			;;
			n|no|N|NO)
				echo -ne "$Y"
				echo -e "ABORTED BY USER$X"
			;;
		esac
	fi	
	
}

finished() {
	echo -e "$R----------------------------------------------------$G"
	echo -e "FINISHED!$Y"
	echo -e "$R----------------------------------------------------$Y"
	echo -e "VERIFY THAT YOUR BACKUP FILES ARE OK!"
	echo -e "TO MOVE YOUR FILES YOU CAN USE:$X" 
	echo -e "scp [source] [destination]$Y"
}

case $ans in
	y|yes|Y|YES)
		backup
		zipping
		change
		finished
	;;
	n|no|N|NO)
		echo -ne "$Y"
		echo -e "ABORTED BY USER$X"
	;;
esac

echo -e "$R----------------------------------------------------$X"
